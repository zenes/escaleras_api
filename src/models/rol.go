package models

/*
	Este paquete conrolene todos los modelos
	en este caso conrolene el modelo de los roles.
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Rol struct {
	Id     int    `db:"id" json:"id"`
	Nombre string `db:"nombre" json:"nombre"`
}

/*
	Esta funcion retorna el rol por ID
*/
func (rol *Rol) GetId() error {
	con.StartConnection()

	sql := `SELECT id, nombre 
	FROM rol WHERE id = ?`
	err := con.Db.Get(rol, sql, rol.Id)
	if err != nil {
		log.Print("No se encontró el rol: ")
		log.Println(err)
	}
	return err
}

/*
	Esta funcion retorna todos los roles

	@param limite int: esta variable recibe un limite
	por si la api rolene que retornar solamente un cierto numero
	de resultados.
*/
func (rol *Rol) GetAll(limite int) ([]Rol, error) {
	con.StartConnection()

	sql := `SELECT id, nombre FROM informacion 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Rol{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, roles no encontrados: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion agrega un nuevo rolpo de inicio a la base de datos
*/

func (rol *Rol) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO rol (nombre) VALUES (:nombre)`
	res, err := tx.NamedExec(sql, &rol)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el rol no pudo ser creado: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de un rol
*/
func (rol *Rol) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE rol SET 
	nombre = :nombre WHERE id = :id`
	_, err := tx.NamedExec(sql, &rol)
	if err != nil {
		log.Print("Error, el rol no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un rol
*/

func (rol *Rol) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM rol WHERE id = :id`
	res, err := tx.NamedExec(sql, &rol)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el rol no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
