package models

/*
	Este paquete conappene todos los modelos
	en este caso conappene el modelo de los apps
*/

import (
	"log"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type App struct {
	Id      int    `db:"id" json:"id"`
	TokenId string `db:"tokenId" json:"tokenId"`
	Secret  string `db:"secret" json:"token"`
}

/*
	Esta funcion retorna el app por ID
*/
func (app *App) GetId() error {
	con.StartConnection()

	sql := `SELECT id, tokenId, secret 
	FROM app WHERE id = ?`
	err := con.Db.Get(app, sql, app.Id)
	if err != nil {
		log.Print("No se encontró el app: ")
		log.Println(err)
		return err
	}
	return nil
}

/*
	Esta funcion retorna la app por token_id
*/
func (app *App) GetByToken() error {
	con.StartConnection()

	sql := `SELECT id, tokenId, secret 
	FROM app WHERE tokenId = ?`
	err := con.Db.Get(app, sql, app.TokenId)
	if err != nil {
		log.Print("No se encontró el app: ")
		log.Println(err)
		return err
	}
	return nil
}
