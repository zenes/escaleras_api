package models

/*
	Este paquete conrespuestasene todos los modelos
	en este caso conrespuestasene el modelo de los respuestass
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Respuestas struct {
	Id          int    `db:"id" json:"id"`
	PreguntasId int    `db:"preguntas_id" json:"preguntas_id"`
	Contenido   string `db:"contenido" json:"contenido"`
	Correcta    int    `db:"correcta" json:"correcta"`

	Preguntas Preguntas `db:"preguntas" json:"preguntas"`
}

/*
	Esta funcion retorna el respuestas por ID
*/
func (resp *Respuestas) GetId() error {
	con.StartConnection()

	sql := `SELECT id, preguntas_id, contenido, correcta 
	FROM respuestas WHERE id = ?`
	err := con.Db.Get(&resp, sql, resp.Id)
	if err != nil {
		log.Print("No se encontró el respuestas: ")
		log.Println(err)
		return err
	}
	resp.Preguntas.Id = resp.PreguntasId
	_ = resp.Preguntas.GetId()
	return nil
}

/*
	Esta funcion retorna la pregunta por ID de pregunta
*/
func (respuestas *Respuestas) GetByPregunta() ([]Respuestas, error) {
	con.StartConnection()
	res := []Respuestas{}

	sql := `SELECT id, preguntas_id, contenido, correcta 
	FROM respuestas WHERE preguntas_id = ?`
	err := con.Db.Select(&res, sql, respuestas.PreguntasId)
	if err != nil {
		log.Print("No se encontraron las respuestas: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion retorna todas los respuestass

	@param limite int: esta variable recibe un limite
	por si la api respuestasene que retornar solamente un cierto numero
	de resultados.
*/
func (respuestas *Respuestas) GetAll(limite int) ([]Respuestas, error) {
	con.StartConnection()

	sql := `SELECT id, preguntas_id, contenido, correcta FROM respuestas 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Respuestas{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, respuestass no encontrados: ")
		log.Println(err)
		return nil, err
	}
	for i := range res {
		res[i].Preguntas.Id = res[i].PreguntasId
		_ = res[i].Preguntas.GetId()
	}
	return res, err
}

/*
	Esta funcion agrega una nueva respuestas a la base de datos
*/

func (respuestas *Respuestas) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO respuestas (
		preguntas_id,
		contenido,
		correcta
	) VALUES (
		:preguntas_id,
		:contenido,
		:correcta
		)`
	res, err := tx.NamedExec(sql, &respuestas)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el respuestas no pudo ser creado: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de un respuestas
*/
func (respuestas *Respuestas) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE respuestas SET 
	preguntas_id = :preguntas_id, 
	contenido = :contenido, 
	correcta = :correcta 
	WHERE id = :id`
	_, err := tx.NamedExec(sql, &respuestas)
	if err != nil {
		log.Print("Error, las respuestas no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un respuestas
*/

func (respuestas *Respuestas) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM respuestas WHERE id = :id`
	res, err := tx.NamedExec(sql, &respuestas)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el respuestas no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
