package models

/*
	Este paquete contiene todos los modelos
	en este caso contiene el modelo de las instituciones.
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Institucion struct {
	Id       int    `db:"id" json:"id"`
	Nombre   string `db:"nombre" json:"nombre"`
	Ciudad   string `db:"ciudad" json:"ciudad"`
	Aprobada int    `db:"aprobada" json:"aprobada"`
}

/*
	Esta funcion retorna la institucion por ID
*/
func (ins *Institucion) GetId() error {
	con.StartConnection()

	sql := `SELECT id, nombre, ciudad, aprobada 
	FROM institucion WHERE id = ?`
	err := con.Db.Get(&ins, sql, ins.Id)
	if err != nil {
		log.Print("No se encontró la institución: ")
		log.Println(err)
	}
	return err
}

/*
	Esta funcion retorna todas las instituciones

	@param limite int: esta variable recibe un limite
	por si la api tiene que retornar solamente un cierto numero
	de resultados.
*/
func (ins *Institucion) GetAll(limite int) ([]Institucion, error) {
	con.StartConnection()

	sql := `SELECT id, nombre, ciudad, aprobada FROM institucion 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Institucion{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, instituciones no encontradas: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion agrega una nueva institucion a la base de datos
*/

func (ins *Institucion) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO institucion (
			nombre, 
			ciudad,
			aprobada
		 ) VALUES (
			:nombre, 
			:ciudad,
			:aprobada
			)`
	res, err := tx.NamedExec(sql, &ins)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la institucion no pudo ser creada: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de una institucion
*/
func (ins *Institucion) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE institucion SET 
		nombre = :nombre, 
		ciudad = :ciudad 
		WHERE id = :id`
	_, err := tx.NamedExec(sql, &ins)
	if err != nil {
		log.Print("Error, la institucion no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion cambia el estado de una institucion.

	Aprobada = 1
	No aprobada = 0
*/
func (ins *Institucion) Aprobar() error {
	con.StartConnection()

	if ins.Aprobada > 1 {
		ins.Aprobada = 1
	}

	tx := con.Db.MustBegin()
	sql := `UPDATE institucion SET 
		aprobada = :aprobada 
		WHERE id = :id`
	_, err := tx.NamedExec(sql, &ins)
	if err != nil {
		log.Print("Error, la institucion no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina una institucion
*/

func (ins *Institucion) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM institucion WHERE id = :id`
	res, err := tx.NamedExec(sql, &ins)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la institucion no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
