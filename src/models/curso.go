package models

/*
	Este paquete concursoene todos los modelos
	en este caso concursoene el modelo de los cursos
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Curso struct {
	Id            int    `db:"id" json:"id"`
	InstitucionId int    `db:"institucion_id" json:"institucion_id"`
	Nombre        string `db:"nombre" json:"nombre"`
	Grado         string `db:"grado" json:"grado"`

	Institucion Institucion `db:"institucion" json:"institucion"`
}

/*
	Esta funcion retorna el curso por ID
*/
func (curso *Curso) GetId() error {
	con.StartConnection()

	sql := `SELECT id, institucion_id, nombre, grado 
	FROM curso WHERE id = ?`
	err := con.Db.Get(&curso, sql, curso.Id)
	if err != nil {
		log.Print("No se encontró el curso: ")
		log.Println(err)
		return err
	}
	curso.Institucion.Id = curso.InstitucionId
	_ = curso.Institucion.GetId()
	return nil
}

/*
	Esta funcion retorna la pregunta por ID de institucion
*/
func (curso *Curso) GetByInstitucion() ([]Curso, error) {
	con.StartConnection()
	res := []Curso{}

	sql := `SELECT id, institucion_id, nombre, grado 
FROM curso WHERE institucion_id = ?`
	err := con.Db.Select(&res, sql, curso.InstitucionId)
	if err != nil {
		log.Print("No se encontraron los cursos: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion retorna todas los cursos

	@param limite int: esta variable recibe un limite
	por si la api cursoene que retornar solamente un cierto numero
	de resultados.
*/
func (curso *Curso) GetAll(limite int) ([]Curso, error) {
	con.StartConnection()

	sql := `SELECT id, institucion_id, nombre, grado FROM curso 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Curso{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, cursos no encontrados: ")
		log.Println(err)
		return nil, err
	}
	for i := range res {
		res[i].Institucion.Id = res[i].InstitucionId
		_ = res[i].Institucion.GetId()
	}
	return res, err
}

/*
	Esta funcion agrega una nueva curso a la base de datos
*/

func (curso *Curso) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO curso (
		institucion_id,
		nombre,
		grado
	) VALUES (
		:institucion_id,
		:nombre,
		:grado
		)`
	res, err := tx.NamedExec(sql, &curso)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el curso no pudo ser creado: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de un curso
*/
func (curso *Curso) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE curso SET 
	institucion_id = :institucion_id,
	nombre = :nombre,
	grado = :grado 
	WHERE id = :id`
	_, err := tx.NamedExec(sql, &curso)
	if err != nil {
		log.Print("Error, el curso no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un curso
*/

func (curso *Curso) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM curso WHERE id = :id`
	res, err := tx.NamedExec(sql, &curso)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el curso no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
