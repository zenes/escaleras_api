package models

import "testing"

func TestApp_GetByToken(t *testing.T) {
	type fields struct {
		Id      int
		TokenId string
		Secret  string
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "TestApp_GetByToken 01",
			fields: fields{
				TokenId: "de7550363fdf32800b0fb14d",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			app := &App{
				Id:      tt.fields.Id,
				TokenId: tt.fields.TokenId,
				Secret:  tt.fields.Secret,
			}
			if err := app.GetByToken(); (err != nil) != tt.wantErr {
				t.Errorf("App.GetByToken() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
