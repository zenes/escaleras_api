package models

/*
	Este paquete conUsuarioCursoene todos los modelos

	En este archivo se ecuentran los metodos SELECT
	para la tabla usuario_has_curso
*/

import (
	"log"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type UsuarioCurso struct {
	UsuarioId int `db:"usuario_id" json:"usuario_id"`
	CursoId   int `db:"curso_id" json:"curso_id"`

	Usuario Usuario `db:"usuario" json:"usuario"`
	Curso   Curso   `db:"curso" json:"curso"`
}

/*
	Esta funcion retorna los cursos por id de usuario
*/
func (usCurso *UsuarioCurso) GetCursos() ([]UsuarioCurso, error) {
	con.StartConnection()

	res := []UsuarioCurso{}

	sql := `SELECT curso_id FROM usuario_has_curso WHERE usuario_id = ?`
	err := con.Db.Select(&res, sql, usCurso.UsuarioId)
	if err != nil {
		log.Print("No se encontraron los cursos")
		log.Println(err)
		return nil, err
	}
	for i := range res {
		res[i].Curso.Id = res[i].CursoId
		_ = res[i].Curso.GetId()
	}
	return res, err
}

/*
	Esta funcion retorna los usuarios por id de curso
*/
func (usCurso *UsuarioCurso) GetUsuarios() ([]UsuarioCurso, error) {
	con.StartConnection()

	res := []UsuarioCurso{}

	sql := `SELECT usuario_id FROM usuario_has_curso WHERE curso_id = ?`
	err := con.Db.Select(&res, sql, usCurso.CursoId)
	if err != nil {
		log.Print("No se encontraron los usuarios")
		log.Println(err)
		return nil, err
	}
	for i := range res {
		res[i].Usuario.Id = res[i].UsuarioId
		_ = res[i].Usuario.GetId()
	}
	return res, err
}
