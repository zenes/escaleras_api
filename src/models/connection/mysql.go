package connection

/*
	Este paquete es el que se encarga de la conexxion a la
	base de datos
*/
import (
	"log"

	// se importa en blanco _ debido a que se usa para la conexión
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

// Db es una variable que se declara exportada para que se pueda usar en otras clases
var Db *sqlx.DB

const (
	dbName     = "escaleras"
	dbUser     = "admin"
	dbPassword = ""
)

// StartConnection se encarga de iniciar la conexión a la base de datos
func StartConnection() {
	var err error

	Db, err = sqlx.Open("mysql", dbUser+":"+dbPassword+"@tcp(127.0.0.1:3306)/"+dbName+"?parseTime=false")

	if err != nil {
		log.Println("No se puede conectar a la base de datos: ")
		log.Panic(err)
	} else {
		err = Db.Ping()
		if err != nil {
			log.Println("La base de datos no se encuentra conectada: ")
			log.Panic(err)
		} else {
			Db.SetConnMaxLifetime(60)
			log.Println("Conexión a BD.")
		}
	}
}
