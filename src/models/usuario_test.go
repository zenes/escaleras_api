package models

import (
	"fmt"
	"testing"
)

func TestUsuario_genClave(t *testing.T) {
	type fields struct {
		Id            int
		RolId         int
		ConfTableroId int
		Nombres       string
		Apellidos     string
		Genero        genero
		NivelStudio   string
		FechaNac      string
		Email         string
		Clave         string
		FechaReg      string
		ActivacionUrl string
		Estado        int
		Rol           Rol
		ConfTablero   ConfTablero
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name: "TestUsuario_genClave 1",
			fields: fields{
				Clave: "123456",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			usuario := &Usuario{
				Id:            tt.fields.Id,
				RolId:         tt.fields.RolId,
				ConfTableroId: tt.fields.ConfTableroId,
				Nombres:       tt.fields.Nombres,
				Apellidos:     tt.fields.Apellidos,
				Genero:        tt.fields.Genero,
				NivelStudio:   tt.fields.NivelStudio,
				FechaNac:      tt.fields.FechaNac,
				Email:         tt.fields.Email,
				Clave:         tt.fields.Clave,
				FechaReg:      tt.fields.FechaReg,
				ActivacionUrl: tt.fields.ActivacionUrl,
				Estado:        tt.fields.Estado,
				Rol:           tt.fields.Rol,
				ConfTablero:   tt.fields.ConfTablero,
			}
			usuario.genClave()
			fmt.Println(usuario)
		})
	}
}
