package models

/*
	Este paquete contiene todos los modelos
	en este caso contiene el modelo de los tipos de juego.
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type TipoInicio struct {
	Id     int    `db:"id" json:"id"`
	Nombre string `db:"nombre" json:"nombre"`
}

/*
	Esta funcion retorna el tipo de inicio por ID
*/
func (ti *TipoInicio) GetId() error {
	con.StartConnection()

	sql := `SELECT id, nombre 
	FROM tipoInicio WHERE id = ?`
	err := con.Db.Get(&ti, sql, ti.Id)
	if err != nil {
		log.Print("No se encontró el tipo de inicio: ")
		log.Println(err)
	}
	return err
}

/*
	Esta funcion retorna todas los tipos de inicio

	@param limite int: esta variable recibe un limite
	por si la api tiene que retornar solamente un cierto numero
	de resultados.
*/
func (ti *TipoInicio) GetAll(limite int) ([]TipoInicio, error) {
	con.StartConnection()

	sql := `SELECT id, nombre FROM tipoInicio 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []TipoInicio{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, tipos de inicio no encontrados: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion agrega un nuevo tipo de inicio a la base de datos
*/

func (ti *TipoInicio) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO tipoInicio (nombre) VALUES (:nombre)`
	res, err := tx.NamedExec(sql, &ti)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el tipo de inicio no pudo ser creado: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de una tipoInicio
*/
func (ti *TipoInicio) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE tipoInicio SET 
	nombre = :nombre WHERE id = :id`
	_, err := tx.NamedExec(sql, &ti)
	if err != nil {
		log.Print("Error, el tipo de inicio no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina una tipoInicio
*/

func (ti *TipoInicio) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM tipoInicio WHERE id = :id`
	res, err := tx.NamedExec(sql, &ti)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el tipo de inicio no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
