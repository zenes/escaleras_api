package models

/*
	Este paquete conusuarioene todos los modelos
	en este caso conusuarioene el modelo de los usuarios.
*/

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"log"
	"math/rand"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
	"golang.org/x/crypto/bcrypt"
)

type genero string

const (
	Hombre genero = "hombre"
	Mujer  genero = "mujer"
	Otro   genero = "otro"
)

type Usuario struct {
	Id            int    `db:"id" json:"id"`
	RolId         int    `db:"rol_id" json:"rol_id"`
	ConfTableroId int    `db:"confTablero_id" json:"conf_tablero_id"`
	Nombres       string `db:"nombres" json:"nombres"`
	Apellidos     string `db:"apellidos" json:"apellidos"`
	Genero        genero `db:"genero" json:"genero"`
	NivelStudio   string `db:"nivelEstudio" json:"nivel_estudio"`
	FechaNac      string `db:"fechaNac" json:"fecha_nac"`
	Email         string `db:"email" json:"email"`
	Clave         string `db:"clave" json:"clave"`
	FechaReg      string `db:"fechaReg" json:"fecha_reg"`
	ActivacionUrl string `db:"activacionUrl" json:"activacion_url"`
	Estado        int    `db:"estado" json:"estado"`

	Rol         Rol         `db:"rol" json:"rol"`
	ConfTablero ConfTablero `db:"confTablero" json:"conf_tablero"`
}

/*
	Esta funcion genera la llave privada de la contraseña
*/
func (usuario *Usuario) genClave() {
	hash, _ := bcrypt.GenerateFromPassword([]byte(usuario.Clave), bcrypt.MinCost)
	usuario.Clave = string(hash)
}

/*
	Esta funcion genera el código de confirmación del correo electrónico
*/
func (usuario *Usuario) genActivacion() {
	hash := sha1.New()
	random := fmt.Sprint(rand.Intn(99999))
	hash.Write([]byte(usuario.Email + random))
	usuario.ActivacionUrl = hex.EncodeToString(hash.Sum(nil))
}

/*
	Esta función la utiliza el email para iniciar
	sesion.
*/
func (usuario *Usuario) Login(claveLogin string) error {
	con.StartConnection()

	sql := `SELECT id, rol_id, confTablero_id, nombres, 
	apellidos, email, clave, genero, nivelEstudio
	FROM usuario WHERE email = ? AND estado = 1 
	AND activacionUrl = 0`
	err := con.Db.Get(usuario, sql, usuario.Email)
	if err != nil {
		log.Print("No se encontró el usuario: ")
		log.Println(err)
		return err
	}

	usuario.Rol.Id = usuario.RolId
	_ = usuario.Rol.GetId()

	usuario.ConfTablero.Id = usuario.ConfTableroId
	_ = usuario.ConfTablero.GetId()

	err = bcrypt.CompareHashAndPassword([]byte(usuario.Clave), []byte(claveLogin))
	if err != nil {
		log.Print("Contraseña invalida")
		return err
	}

	return nil
}

/*
	Esta funcion retorna el usuario por ID
*/
func (usuario *Usuario) GetId() error {
	con.StartConnection()

	sql := `SELECT id, rol_id, confTablero_id, nombres, 
	apellidos, genero, nivelEstudio,fechaNac, email, fechaReg,
	activacionUrl, estado
	FROM usuario WHERE id = ?`
	err := con.Db.Get(&usuario, sql, usuario.Id)
	if err != nil {
		log.Print("No se encontró el usuario: ")
		log.Println(err)
	}
	usuario.Rol.Id = usuario.RolId
	_ = usuario.Rol.GetId()
	usuario.ConfTablero.Id = usuario.ConfTableroId
	_ = usuario.ConfTablero.GetId()
	return err
}

/*
	Esta funcion retorna el usuario por Email
*/
func (usuario *Usuario) GetByEmail() error {
	con.StartConnection()

	sql := `SELECT id, rol_id, confTablero_id, nombres, 
	apellidos, genero, nivelEstudio,fechaNac, email, fechaReg,
	activacionUrl, estado
	FROM usuario WHERE email = ?`
	err := con.Db.Get(&usuario, sql, usuario.Email)
	if err != nil {
		log.Print("No se encontró el usuario: ")
		log.Println(err)
	}
	usuario.Rol.Id = usuario.RolId
	_ = usuario.Rol.GetId()
	usuario.ConfTablero.Id = usuario.ConfTableroId
	_ = usuario.ConfTablero.GetId()
	return err
}

/*
	Esta funcion retorna el usuario solo si es admin
*/
func (usuario *Usuario) GetByEmailAdmin() error {
	con.StartConnection()

	sql := `SELECT id, rol_id, confTablero_id, nombres, 
	apellidos, genero, nivelEstudio,fechaNac, email, fechaReg,
	activacionUrl, estado
	FROM usuario WHERE email = ? AND rol_id = 3`
	err := con.Db.Get(&usuario, sql, usuario.Email)
	if err != nil {
		log.Print("No se encontró el usuario: ")
		log.Println(err)
	}
	usuario.Rol.Id = usuario.RolId
	_ = usuario.Rol.GetId()
	usuario.ConfTablero.Id = usuario.ConfTableroId
	_ = usuario.ConfTablero.GetId()
	return err
}

/*
	Esta funcion retorna todos los usuarios

	@param limite int: esta variable recibe un limite
	por si la api usuarioene que retornar solamente un cierto numero
	de resultados.
*/
func (usuario *Usuario) GetAll(limite string) ([]Usuario, error) {
	con.StartConnection()

	sql := `SELECT id, rol_id, confTablero_id, nombres, 
	apellidos, genero, nivelEstudio, fechaNac, email, fechaReg,
	activacionUrl, estado 
	FROM usuario
	ORDER BY id ASC`

	if limite != "" {
		sql += " LIMIT " + limite
	}

	res := []Usuario{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, usuarios no encontrados: ")
		log.Println(err)
		return nil, err
	}

	for i := range res {
		res[i].Rol.Id = res[i].RolId
		_ = res[i].Rol.GetId()
		res[i].ConfTablero.Id = res[i].ConfTableroId
		_ = res[i].ConfTablero.GetId()
	}
	return res, err
}

/*
	Esta funcion agrega un nuevo usuariopo de inicio a la base de datos
*/

func (usuario *Usuario) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	usuario.genClave()
	// Se desactiva por el momento
	// puede ser usado cuando se quiera enviar email de confirmacion.
	// usuario.genActivacion()

	tx := con.Db.MustBegin()
	sql := `INSERT INTO usuario (
		rol_id,
		confTablero_id,
		nombres,
		apellidos,
		genero,
		nivelEstudio,
		fechaNac,
		email,
		clave,
		activacionUrl
	) VALUES (
		:rol_id,
		:confTablero_id,
		:nombres,
		:apellidos,
		:genero,
		:nivelEstudio,
		:fechaNac,
		:email,
		:clave,
		:activacionUrl
	)`
	res, err := tx.NamedExec(sql, &usuario)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el usuario no pudo ser creado: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de un usuario
*/
func (usuario *Usuario) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE usuario SET 
		rol_id = :rol_id,
		confTablero_id = :confTablero_id,
		nombres = :nombres,
		apellidos = :apellidos,
		genero = :genero,
		nivelEstudio = :nivelEstudio,
		fechaNac = :fechaNac,
		email = :email
		WHERE id = :id`
	_, err := tx.NamedExec(sql, &usuario)
	if err != nil {
		log.Print("Error, el usuario no ha sido actualizado: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion cambia el estado de un usuario
*/
func (usuario *Usuario) CambioEstado() error {
	con.StartConnection()

	if usuario.Estado > 1 {
		usuario.Estado = 1
	}

	tx := con.Db.MustBegin()
	sql := `UPDATE usuario SET 
		estado = :estado
		WHERE id = :id`
	_, err := tx.NamedExec(sql, &usuario)
	if err != nil {
		log.Print("Error, el usuario no ha sido actualizado: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion cambia la contraseña de un usuario
*/
func (usuario *Usuario) CambioClave(claveNueva string) error {
	con.StartConnection()

	err := bcrypt.CompareHashAndPassword([]byte(usuario.Clave), []byte(claveNueva))
	if err == nil {
		log.Print("La nueva contraseña es igual a la anterior")
		return err
	}

	usuario.Clave = claveNueva
	usuario.genClave()

	tx := con.Db.MustBegin()
	sql := `UPDATE usuario SET 
		clave = :clave
		WHERE id = :id`
	_, err = tx.NamedExec(sql, &usuario)
	if err != nil {
		log.Print("Error, la contraseña no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un usuario
*/

func (usuario *Usuario) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM usuario WHERE id = :id`
	res, err := tx.NamedExec(sql, &usuario)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el usuario no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
