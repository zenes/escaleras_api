package models

/*
	Este paquete contematicaene todos los modelos
	en este caso contematicaene el modelo de las tematicas
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Tematica struct {
	Id          int    `db:"id" json:"id"`
	Descripcion string `db:"descripcion" json:"descripcion"`
}

/*
	Esta funcion retorna el tematica por ID
*/
func (inf *Tematica) GetId() error {
	con.StartConnection()

	sql := `SELECT id, descripcion 
	FROM tematica WHERE id = ?`
	err := con.Db.Get(&inf, sql, inf.Id)
	if err != nil {
		log.Print("No se encontró la tematica: ")
		log.Println(err)
	}
	return err
}

/*
	Esta funcion retorna todas las tematicas

	@param limite int: esta variable recibe un limite
	por si la api tematicaene que retornar solamente un cierto numero
	de resultados.
*/
func (tematica *Tematica) GetAll(limite int) ([]Tematica, error) {
	con.StartConnection()

	sql := `SELECT id, descripcion FROM tematica 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Tematica{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, tematicas no encontradas: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion agrega una nueva tematica a la base de datos
*/

func (tematica *Tematica) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO tematica (descripcion) VALUES (:descripcion)`
	res, err := tx.NamedExec(sql, &tematica)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la tematica no pudo ser creada: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de un tematica
*/
func (tematica *Tematica) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE tematica SET 
	descripcion = :descripcion WHERE id = :id`
	_, err := tx.NamedExec(sql, &tematica)
	if err != nil {
		log.Print("Error, la tematica no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un tematica
*/

func (tematica *Tematica) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM tematica WHERE id = :id`
	res, err := tx.NamedExec(sql, &tematica)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la tematica no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
