package models

/*
	Este paquete conpreguntaene todos los modelos
	en este caso conpreguntaene el modelo de los preguntas
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Preguntas struct {
	Id         int    `db:"id" json:"id"`
	TematicaId int    `db:"tematica_id" json:"tematica_id"`
	Contenido  string `db:"contenido" json:"contenido"`
	Dificultad string `db:"dificultad" json:"dificultad"`

	Tematica Tematica `db:"tematica" json:"tematica"`
}

/*
	Esta funcion retorna la pregunta por ID
*/
func (preg *Preguntas) GetId() error {
	con.StartConnection()

	sql := `SELECT id, tematica_id, contenido, dificultad 
	FROM preguntas WHERE id = ?`
	err := con.Db.Get(&preg, sql, preg.Id)
	if err != nil {
		log.Print("No se encontró la pregunta: ")
		log.Println(err)
		return err
	}
	preg.Tematica.Id = preg.TematicaId
	_ = preg.Tematica.GetId()
	return nil
}

/*
	Esta funcion retorna la pregunta por ID de tematica
*/
func (preg *Preguntas) GetByTematica() ([]Preguntas, error) {
	con.StartConnection()
	res := []Preguntas{}

	sql := `SELECT id, tematica_id, contenido, dificultad 
	FROM preguntas WHERE tematica_id = ?`
	err := con.Db.Select(&res, sql, preg.TematicaId)
	if err != nil {
		log.Print("No se encontraron las preguntas: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion retorna todas los preguntas

	@param limite int: esta variable recibe un limite
	por si la api preguntaene que retornar solamente un cierto numero
	de resultados.
*/
func (pregunta *Preguntas) GetAll(limite int) ([]Preguntas, error) {
	con.StartConnection()

	sql := `SELECT id, tematica_id, contenido, dificultad FROM preguntas 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Preguntas{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, preguntas no encontradas: ")
		log.Println(err)
		return nil, err
	}
	for i := range res {
		res[i].Tematica.Id = res[i].TematicaId
		_ = res[i].Tematica.GetId()
	}
	return res, err
}

/*
	Esta funcion agrega una nueva pregunta a la base de datos
*/

func (pregunta *Preguntas) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO preguntas (
		tematica_id,
		contenido,
		dificultad
	) VALUES (
		:tematica_id,
		:contenido,
		:dificultad
		)`
	res, err := tx.NamedExec(sql, &pregunta)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la pregunta no pudo ser creado: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de un pregunta
*/
func (pregunta *Preguntas) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE preguntas SET 
	tematica_id = :tematica_id,
	contenido = :contenido,
	dificultad = :dificultad
	WHERE id = :id`
	_, err := tx.NamedExec(sql, &pregunta)
	if err != nil {
		log.Print("Error, la pregunta no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un pregunta
*/

func (pregunta *Preguntas) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM preguntas WHERE id = :id`
	res, err := tx.NamedExec(sql, &pregunta)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la pregunta no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
