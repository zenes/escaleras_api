package models

/*
	Este paquete conconfTableroene todos los modelos
	en este caso contconfTableroa el modelo de la configuracion.
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type ConfTablero struct {
	Id              int    `db:"id" json:"id"`
	Nombre          string `db:"nombre" json:"nombre"`
	ColorPrincipal  string `db:"colorPrincipal" json:"color_principal"`
	ColorSecundario string `db:"colorSecundario" json:"color_secundario"`
}

/*
	Esta funcion retorna la configuracion del tablero por ID
*/
func (conf *ConfTablero) GetId() error {
	con.StartConnection()

	sql := `SELECT id, nombre, colorPrincipal, colorSecundario
	FROM confTablero WHERE id = ?`
	err := con.Db.Get(conf, sql, conf.Id)
	if err != nil {
		log.Print("No se encontró la configuración: ")
		log.Println(err)
	}
	return err
}

/*
	Esta funcion retorna las configuraciones del tablero

	@param limite int: esta variable recibe un limite
	por si la api confTablero que retornar solamente un cierto numero
	de resultados.
*/
func (conf *ConfTablero) GetAll(limite int) ([]ConfTablero, error) {
	con.StartConnection()

	sql := `SELECT id, nombre, colorPrincipal, colorSecundario
	FROM confTablero ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []ConfTablero{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, configuraciones no encontrados: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion agrega una nueva configuración del tablero a la base de datos
*/

func (conf *ConfTablero) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO confTablero (
			nombre, 
			colorPrincipal, 
			colorSecundario 
		) 
		VALUES (
			:nombre, 
			:colorPrincipal, 
			:colorSecundario 
			)`
	res, err := tx.NamedExec(sql, &conf)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la configuracion del tablero no pudo ser creada: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de una configuracion de tablero
*/
func (conf *ConfTablero) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE confTablero SET 
		nombre = :nombre, 
		colorPrincipal = :colorPrincipal, 
		colorSecundario = :colorSecundario 
	WHERE id = :id`
	_, err := tx.NamedExec(sql, &conf)
	if err != nil {
		log.Print("Error, la configuracion del tablero no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un confTablero
*/

func (conf *ConfTablero) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM confTablero WHERE id = :id`
	res, err := tx.NamedExec(sql, &conf)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la configuracion del tablero no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
