package models

/*
	Este paquete conjuegoene todos los modelos
	en este caso conjuegoene el modelo de los juegos
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Juego struct {
	Id           int    `db:"id" json:"id"`
	TipoInicioId int    `db:"tipoInicio_id" json:"tipo_inicio_id"`
	GanadorId    int    `db:"ganador_id" json:"ganador_id"`
	HoraInicio   string `db:"horaInicio" json:"hora_inicio"`
	HoraFin      string `db:"horaFin" json:"hora_fin"`

	TipoInicio TipoInicio `db:"tipo_inicio" json:"tipo_inicio"`
}

/*
	Esta funcion retorna el juego por ID
*/
func (juego *Juego) GetId() error {
	con.StartConnection()

	sql := `SELECT id, tipoInicio_id, ganador_id, horaInicio
	horaFin
	FROM juego WHERE id = ?`
	err := con.Db.Get(&juego, sql, juego.Id)
	if err != nil {
		log.Print("No se encontró el juego: ")
		log.Println(err)
		return err
	}
	juego.TipoInicio.Id = juego.TipoInicioId
	_ = juego.TipoInicio.GetId()
	return err
}

/*
	Esta funcion retorna la pregunta por ID de institucion
*/
func (juego *Juego) GetByTipoInicio() ([]Juego, error) {
	con.StartConnection()
	res := []Juego{}

	sql := `SELECT id, tipoInicio_id, ganador_id, horaInicio
	horaFin 
	FROM juego WHERE tipoInicio_id = ?`
	err := con.Db.Select(&res, sql, juego.TipoInicioId)
	if err != nil {
		log.Print("No se encontraron los juegos: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion retorna todas los juegos

	@param limite int: esta variable recibe un limite
	por si la api juegoene que retornar solamente un cierto numero
	de resultados.
*/
func (juego *Juego) GetAll(limite int) ([]Juego, error) {
	con.StartConnection()

	sql := `SELECT SELECT id, tipoInicio_id, ganador_id, horaInicio
	horaFin	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Juego{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, juegos no encontrados: ")
		log.Println(err)
		return nil, err
	}
	for i := range res {
		res[i].TipoInicio.Id = res[i].TipoInicioId
		_ = res[i].TipoInicio.GetId()
	}
	return res, err
}

/*
	Esta funcion agrega una nueva juego a la base de datos
*/

func (juego *Juego) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO juego (
		tipoInicio_id
	) VALUES (
		:tipoInicio_id
	)`
	res, err := tx.NamedExec(sql, &juego)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el juego no pudo ser creado: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion finaliza un juego
*/
func (juego *Juego) Finalizar() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE juego SET 
	ganador_id = :ganador_id,
	horaFin = :horaFin
	WHERE id = :id`
	_, err := tx.NamedExec(sql, &juego)
	if err != nil {
		log.Print("Error, el juego no podido ser finalizado: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}
