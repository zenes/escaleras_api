package models

/*
	Este paquete coninformacionene todos los modelos
	en este caso coninformacionene el modelo de las cartas de información.
*/

import (
	"log"
	"strconv"

	con "gitlab.com/zenes/escaleras_api/src/models/connection"
)

type Informacion struct {
	Id        int    `db:"id" json:"id"`
	Contenido string `db:"contenido" json:"contenido"`
}

/*
	Esta funcion retorna el informacion por ID
*/
func (inf *Informacion) GetId() error {
	con.StartConnection()

	sql := `SELECT id, contenido 
	FROM informacion WHERE id = ?`
	err := con.Db.Get(&inf, sql, inf.Id)
	if err != nil {
		log.Print("No se encontró la informacion: ")
		log.Println(err)
	}
	return err
}

/*
	Esta funcion retorna todas las cartas de información

	@param limite int: esta variable recibe un limite
	por si la api informacionene que retornar solamente un cierto numero
	de resultados.
*/
func (informacion *Informacion) GetAll(limite int) ([]Informacion, error) {
	con.StartConnection()

	sql := `SELECT id, contenido FROM informacion 
	ORDER BY id ASC`

	if limite != 0 {
		sql += " LIMIT " + strconv.Itoa(limite)
	}

	res := []Informacion{}
	err := con.Db.Select(&res, sql)
	if err != nil {
		log.Print("Error, cartas de información no encontradas: ")
		log.Println(err)
		return nil, err
	}
	return res, err
}

/*
	Esta funcion agrega un nuevo informacionpo de inicio a la base de datos
*/

func (informacion *Informacion) Insert() (int, error) {
	con.StartConnection()
	var id int64
	var r int

	tx := con.Db.MustBegin()
	sql := `INSERT INTO informacion (contenido) VALUES (:contenido)`
	res, err := tx.NamedExec(sql, &informacion)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, el informacion no pudo ser creada: ")
		log.Println(err)
	}
	_ = tx.Commit()
	id, err = res.LastInsertId()
	r = int(id)

	return r, err
}

/*
	Esta funcion actualiza el valor de un informacion
*/
func (informacion *Informacion) Update() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `UPDATE informacion SET 
	contenido = :contenido WHERE id = :id`
	_, err := tx.NamedExec(sql, &informacion)
	if err != nil {
		log.Print("Error, el informacion no ha sido actualizada: ")
		log.Println(err)
	}
	err = tx.Commit()
	return err
}

/*
	Esta funcion elimina un informacion
*/

func (informacion *Informacion) Delete() error {
	con.StartConnection()

	tx := con.Db.MustBegin()
	sql := `DELETE FROM informacion WHERE id = :id`
	res, err := tx.NamedExec(sql, &informacion)
	count, _ := res.RowsAffected()
	if err != nil || count == 0 {
		log.Print("Error, la informacion no se pudo eliminar: ")
		log.Println(err)
		return err
	}
	err = tx.Commit()
	return err
}
