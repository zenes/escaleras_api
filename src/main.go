package main

/*
	En este paquete se inicia toda la app
*/

import (
	"log"
	"net/http"
)

/*
	Esta es la función main de la aplicación, en esta función se inicia
	el servidor de la API
*/
func main() {
	router := NewRouter()
	server := http.ListenAndServe(":8080", router)
	defer recover()
	log.Print(server)
}

func recover() {
	log.Println("Error, se continúa con la ejecución")
}
