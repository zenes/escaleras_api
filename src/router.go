package main

/*
	Este archivo contiene todas las rutas del aplicativo
*/

import (
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/zenes/escaleras_api/src/routes"
)

type path struct {
	nombre     string
	metodo     string
	patron     string
	handleFunc http.HandlerFunc
}

type paths []path

/*
	Estas son las rutas de la aplicación.

	Agregar nuevas rutas aquí
*/
var urlPaths = paths{
	path{
		"Index",
		"GET",
		"/",
		routes.Raiz,
	},
	path{
		"Login",
		"POST",
		"/usuario/login",
		routes.Login,
	},
	path{
		"Usuarios",
		"GET",
		"/usuario/lista/{limite}",
		routes.Usuarios,
	},
	path{
		"RegistroDocente",
		"PUT",
		"/usuario/registro",
		routes.RegistroUsuario,
	},
}

/*
	This function is the responsible of creating the mux
	router with all the routes included in this file
	don't modify it
*/
func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, path := range urlPaths {
		router.Methods(path.metodo).
			Path(path.patron).
			Name(path.nombre).
			Handler(path.handleFunc)
	}
	return router
}
