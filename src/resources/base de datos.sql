-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema escaleras
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema escaleras
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `escaleras` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ;
USE `escaleras` ;

-- -----------------------------------------------------
-- Table `escaleras`.`tematica`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`tematica` (
  `id` TINYINT(2) NOT NULL AUTO_INCREMENT,
  `descripcion` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`preguntas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`preguntas` (
  `id` INT(64) NOT NULL AUTO_INCREMENT,
  `tematica_id` TINYINT(2) NOT NULL,
  `contenido` TEXT(1024) NOT NULL,
  `dificultad` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_preguntas_tematica1_idx` (`tematica_id` ASC) ,
  CONSTRAINT `fk_preguntas_tematica1`
    FOREIGN KEY (`tematica_id`)
    REFERENCES `escaleras`.`tematica` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`respuestas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`respuestas` (
  `id` INT(128) NOT NULL AUTO_INCREMENT,
  `preguntas_id` INT(64) NOT NULL,
  `contenido` TEXT(1024) NOT NULL,
  `correcta` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_respuestas_preguntas1_idx` (`preguntas_id` ASC) ,
  CONSTRAINT `fk_respuestas_preguntas1`
    FOREIGN KEY (`preguntas_id`)
    REFERENCES `escaleras`.`preguntas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`información`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`información` (
  `id` INT(64) NOT NULL AUTO_INCREMENT,
  `contenido` TEXT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`rol` (
  `id` TINYINT(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`institucion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`institucion` (
  `id` INT(64) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(255) NOT NULL,
  `ciudad` VARCHAR(128) NOT NULL,
  `aprobada` TINYINT(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`curso` (
  `id` INT(128) NOT NULL AUTO_INCREMENT,
  `institucion_id` INT(64) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `grado` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_curso_institucion1_idx` (`institucion_id` ASC) ,
  CONSTRAINT `fk_curso_institucion1`
    FOREIGN KEY (`institucion_id`)
    REFERENCES `escaleras`.`institucion` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `escaleras`.`confTablero`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`confTablero` (
  `id` TINYINT(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NOT NULL,
  `colorPrincipal` VARCHAR(45) NOT NULL,
  `colorSecundario` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`usuario` (
  `id` INT(128) NOT NULL AUTO_INCREMENT,
  `rol_id` TINYINT(1) NOT NULL,
  `confTablero_id` TINYINT(1) NOT NULL,
  `nombres` VARCHAR(128) NOT NULL,
  `apellidos` VARCHAR(255) NOT NULL,
  `genero` ENUM('hombre', 'mujer', 'otro') NOT NULL DEFAULT 'otro',
  `nivelEstudio` VARCHAR(255) NOT NULL,
  `fechaNac` DATE NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `clave` VARCHAR(128) NOT NULL,
  `fechaReg` DATETIME NOT NULL DEFAULT NOW(),
  `activacionUrl` VARCHAR(255) NOT NULL,
  `estado` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) ,
  UNIQUE INDEX `clave_UNIQUE` (`clave` ASC) ,
  INDEX `fk_usuario_rol1_idx` (`rol_id` ASC) ,
  INDEX `fk_usuario_confTablero1_idx` (`confTablero_id` ASC) ,
  UNIQUE INDEX `activacionUrl_UNIQUE` (`activacionUrl` ASC) ,
  CONSTRAINT `fk_usuario_rol1`
    FOREIGN KEY (`rol_id`)
    REFERENCES `escaleras`.`rol` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_confTablero1`
    FOREIGN KEY (`confTablero_id`)
    REFERENCES `escaleras`.`confTablero` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = big5;


-- -----------------------------------------------------
-- Table `escaleras`.`usuario_has_curso`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`usuario_has_curso` (
  `usuario_id` INT(128) NOT NULL,
  `curso_id` INT(128) NOT NULL,
  PRIMARY KEY (`usuario_id`, `curso_id`),
  INDEX `fk_usuario_has_curso_curso1_idx` (`curso_id` ASC) ,
  INDEX `fk_usuario_has_curso_usuario1_idx` (`usuario_id` ASC) ,
  CONSTRAINT `fk_usuario_has_curso_usuario1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `escaleras`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_curso_curso1`
    FOREIGN KEY (`curso_id`)
    REFERENCES `escaleras`.`curso` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = big5;


-- -----------------------------------------------------
-- Table `escaleras`.`tipoInicio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`tipoInicio` (
  `id` TINYINT(1) NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`juego`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`juego` (
  `id` INT(164) NOT NULL AUTO_INCREMENT,
  `tipoInicio_id` TINYINT(1) NOT NULL,
  `ganador_id` INT(128) NULL,
  `horaInicio` DATETIME NOT NULL DEFAULT NOW(),
  `horaFin` DATETIME NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_juego_tipoInicio1_idx` (`tipoInicio_id` ASC) ,
  CONSTRAINT `fk_juego_tipoInicio1`
    FOREIGN KEY (`tipoInicio_id`)
    REFERENCES `escaleras`.`tipoInicio` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `escaleras`.`usuario_has_juego`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`usuario_has_juego` (
  `usuario_id` INT(128) NOT NULL,
  `juego_id` INT(164) NOT NULL,
  PRIMARY KEY (`usuario_id`, `juego_id`),
  INDEX `fk_usuario_has_juego_juego1_idx` (`juego_id` ASC) ,
  INDEX `fk_usuario_has_juego_usuario1_idx` (`usuario_id` ASC) ,
  CONSTRAINT `fk_usuario_has_juego_usuario1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `escaleras`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_juego_juego1`
    FOREIGN KEY (`juego_id`)
    REFERENCES `escaleras`.`juego` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = big5;


-- -----------------------------------------------------
-- Table `escaleras`.`app`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `escaleras`.`app` (
  `id` TINYINT(2) NOT NULL AUTO_INCREMENT,
  `tokenId` VARCHAR(128) NOT NULL,
  `secret` VARCHAR(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `tokenId_UNIQUE` (`tokenId` ASC) ,
  UNIQUE INDEX `secret_UNIQUE` (`secret` ASC) )
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
