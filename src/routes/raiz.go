package routes

/*
	Este archivo contiene la ruta raiz de la aplicación
*/
import (
	"net/http"
)

/*
	@param w Esta variable contiene el elemento http que escribe la respuesta
	@param r Esta variable contiene el elemento http que recibe la pregunta
*/
func Raiz(w http.ResponseWriter, r *http.Request) {
	response(w, 404, "En construcción")
}
