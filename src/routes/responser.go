package routes

import (
	"fmt"
	"net/http"
	"strconv"
)

/*
	Esta funcion se encarga de escribir las respuestas
*/
func response(w http.ResponseWriter, status int, result string) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	if status != 200 && status != 201 {
		code := strconv.Itoa(status)
		result = fmt.Sprintf(`{
				"status":"%v",
				"message":"%v"
			}`,
			code,
			result,
		)
	}

	fmt.Fprint(w, result)
}
