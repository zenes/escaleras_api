package routes

/*
	Este archivo contiene la ruta raiz de la aplicación
*/
import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/zenes/escaleras_api/src/auth"
	mod "gitlab.com/zenes/escaleras_api/src/models"
)

/*
	Esta función returna el login del usuario

	@param w Esta variable contiene el elemento http que escribe la respuesta
	@param r Esta variable contiene el elemento http que recibe la pregunta
*/
func Login(w http.ResponseWriter, r *http.Request) {
	appToken, _ := auth.CapturaAuth(r)

	if appToken == "" {
		response(w, 401, "Error no se enviaron tokens de autorización")
		return
	}

	decoder := json.NewDecoder(r.Body)
	usuario := mod.Usuario{}
	err := decoder.Decode(&usuario)

	if usuario.Login(usuario.Clave) != nil {
		response(w, 401, "Error, usuario no encontrado o problema de contraseña")
		return
	}

	if err != nil {
		response(w, 401, "No se pudo autorizar al usuario")
		return
	}

	type resp struct {
		Nombre   string `json:"nombre"`
		Apellido string `json:"apellido"`
		Email    string `json:"email"`
		Genero   string `json:"genero"`
		Token    string `json:"token"`
	}

	tokenResp, err := auth.CrearToken(appToken, usuario.Nombres+" "+usuario.Apellidos,
		usuario.RolId, usuario.Email)

	if err != nil {
		response(w, 401, "No se pudo autorizar al usuario")
		return
	}

	res := resp{}
	res.Nombre = usuario.Nombres
	res.Apellido = usuario.Apellidos
	res.Email = usuario.Email
	res.Genero = string(usuario.Genero)
	res.Token = tokenResp

	respuesta, _ := json.Marshal(res)

	response(w, 200, string(respuesta))
}

/*
	Esta funcion retorna la lista de usuarios
	en caso de que se envie un limite se limita la lista
*/
func Usuarios(w http.ResponseWriter, r *http.Request) {
	claims := auth.JwtClaim{}
	token, err := claims.ValidaAuth(r)
	if err != nil {
		response(w, 401, err.Error())
		return
	}
	usuario := mod.Usuario{}

	params := mux.Vars(r)
	limite := params["limite"]

	usuarios, err := usuario.GetAll(limite)
	if err != nil {
		response(w, 401, err.Error())
		return
	}

	type resp struct {
		Usuarios []mod.Usuario `json:"usuarios"`
		Token    string        `json:"token"`
	}

	dataRes := resp{}
	dataRes.Usuarios = usuarios
	dataRes.Token = token

	respuesta, err := json.Marshal(dataRes)
	if err != nil {
		response(w, 401, err.Error())
		return
	}

	response(w, 200, string(respuesta))
}

func RegistroUsuario(w http.ResponseWriter, r *http.Request) {
	claims := auth.JwtClaim{}
	token, err := claims.ValidaAuth(r)
	if err != nil {
		response(w, 401, err.Error())
		return
	}
	decoder := json.NewDecoder(r.Body)
	usuario := mod.Usuario{}
	err = decoder.Decode(&usuario)
	if err != nil {
		response(w, 401, err.Error())
		return
	}
	if usuario.RolId == 3 && claims.Rol != 3 {
		usuario.GetByEmailAdmin()
	}
	usuario.Insert()
	usuario.ActivacionUrl = "0"
	if err != nil {
		response(w, 401, err.Error())
		return
	}
	type resp struct {
		Usuario mod.Usuario `json:"usuario"`
		Token   string      `json:"token"`
	}
	respData := resp{}
	respData.Usuario = usuario
	respData.Token = token

	respuesta, err := json.Marshal(respData)
	if err != nil {
		response(w, 401, err.Error())
		return
	}
	response(w, 200, string(respuesta))
}
