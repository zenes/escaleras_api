package auth

import (
	"errors"
	"log"
	"net/http"
	"strings"
	"time"

	jwt "github.com/golang-jwt/jwt"

	mod "gitlab.com/zenes/escaleras_api/src/models"
)

// JwtClaim Estructura del objeto claim del jwt
type JwtClaim struct {
	Usuario    string    `json:"user"`
	Rol        int       `json:"rol"`
	Email      string    `json:"email"`
	Autorizado bool      `json:"authorized"`
	ExpiredAt  time.Time `json:"exp"`
	jwt.StandardClaims
}

//
// CrearToken es una funcion que crea el token de autorización con JWT
//
func CrearToken(appToken string, usuario string, rol int, email string) (string, error) {

	app := mod.App{}
	app.TokenId = appToken
	err := app.GetByToken()

	if err != nil {
		return "", err
	}

	token := jwt.New(jwt.SigningMethodHS256)
	valores := token.Claims.(jwt.MapClaims)
	valores["authorized"] = true
	valores["user"] = usuario
	valores["rol"] = rol
	valores["exp"] = time.Now().Add(time.Minute * 60)

	valorToken, err := token.SignedString([]byte(app.Secret))

	if err != nil {
		log.Printf("error, no se pudo crear el token: %v", err.Error())
		return "", err
	}
	return valorToken, err
}

//
// CapturaAuth es una función que captura los tokens de autorización de las
// peticiones http
//
func CapturaAuth(r *http.Request) (string, string) {
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, " ")
	return splitToken[0], splitToken[1]
}

//
// ValidaAuth es una función que valida el token que envía el usuario
//
func (claims *JwtClaim) ValidaAuth(r *http.Request) (res string, err error) {
	err = errors.New("no se pudo validar el acceso al endpoint")
	res = ""

	appTokenId, bearer := CapturaAuth(r)

	app := mod.App{}
	app.TokenId = appTokenId

	app.GetByToken()
	if bearer == "" && appTokenId == "" {
		log.Println("Error, no viene handler: " + appTokenId)
		return
	}

	token, err := jwt.ParseWithClaims(
		bearer,
		&JwtClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(app.Secret), nil
		},
	)

	if err != nil {
		return
	}
	var ok bool
	claims, ok = token.Claims.(*JwtClaim)
	if !ok {
		err = errors.New("no se pudo leer el token")
		return
	}

	if claims.ExpiredAt.Unix() < time.Now().Local().Unix() {
		err = errors.New("JWT is expired")
		return
	}
	usuario := mod.Usuario{}
	usuario.Email = claims.Email
	usuario.GetByEmail()

	return CrearToken(app.TokenId, usuario.Nombres+" "+usuario.Apellidos,
		usuario.RolId, usuario.Email)
}
