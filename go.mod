module gitlab.com/zenes/escaleras_api

go 1.16

require (
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.3
	golang.org/x/crypto v0.0.0-20210415154028-4f45737414dc
)
